<?php

namespace AppBundle\Controller;

use AppBundle\Service\Destructor\App\Captcha\ShaCaptcha;
use AppBundle\Service\Destructor\App\Connectors\RedisConnect;
use AppBundle\Service\Destructor\App\Encrypts\MCrypt;
use AppBundle\Service\Destructor\Destructor;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\BrowserKit\Response;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    private $service;

    public function __construct()
    {
        $connect = new RedisConnect();
        $captcha = new ShaCaptcha();
        $encrypt = new MCrypt();
        $this->service = new Destructor($connect, $captcha, $encrypt);
    }

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir') . '/..') . DIRECTORY_SEPARATOR,
            'host' => $request->getHttpHost()
        ]);
    }

    /**
     * @Route("/send", name="send")
     */
    public function sendAction(Request $request)
    {

        try {
            $key = $this->service->setMessage(
                [
                    "message" => $request->get("message"),
                    "password" => $request->get("password"),
                    "type" => $request->get("type")
                ]
            );
            return $this->json(["key" => $key]);
        } catch (\RedisException $redisException) {
            return $this->json(["error" => $redisException->getMessage()]);
        }
    }

    /**
     * @Route("/message/{hash}", name="message")
     */
    public function getMessageAction(Request $request, $hash)
    {
        $result = [
            "need_pass" => false,
            "hash" => $hash,
            "message" => ""
        ];
        try {
            $result["message"] = $this->service->getMessage($hash, $request->get("password"));
        } catch (\RedisException $redisException) {
            if ($redisException->getCode() == 403) {
                $result["need_pass"] = true;
            } elseif ($redisException->getCode() == 404) {
                $result["message"] = "MESSAGE NOT FOUND";
            }
        } catch (\Exception $exception) {
            return $this->redirectToRoute("homepage");
        }
        return $this->render('default/message.html.twig', $result);

    }
}
