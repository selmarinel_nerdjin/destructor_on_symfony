<?php
/**
 * Created by PhpStorm.
 * User: Selmarinel
 * Date: 18.04.2017
 * Time: 19:22
 */

namespace AppBundle\Service\Destructor\App\Captcha;

interface CaptchaInterface
{
    public function generate();
}